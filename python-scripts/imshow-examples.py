## imshow and related examples
## Date 2019/4/7

import matplotlib.pyplot as plt
import numpy as np
# for jupyter notebook
%matplotlib inline 

M=np.zeros([5,5]) 
M[1,1]=M[2,3]=M[4,4]=1

fig = plt.figure()

#ax  = fig.add_subplot(111)
ax1 = fig.add_subplot(211)
#for i in np.array(range(5))-0.5:
#   ax1.axhline(y=i,c='black')
#   ax1.axvline(x=i,c='black')  
ax1.set_xlim([-0.5,4.5])
ax1.set_ylim([4.5,-0.5])
ax1.set_xticks([-0.5,0.5,1.5,2.5,3.5,4.5])
ax1.set_yticks([-0.5,0.5,1.5,2.5,3.5,4.5])
ax1.set_xticklabels([0,1,2,3,4,5])
ax1.set_yticklabels([0,1,2,3,4,5])
ax1.grid(True)

pos1=ax1.imshow(1-M,cmap=plt.cm.hot,aspect='equal')
fig.colorbar(pos1, ax=ax1)

###

ax2 = fig.add_subplot(212)
ax2.set_aspect(1.0)
ax2.set_xlim([0,5])
ax2.set_ylim([5,0])
#ax2.set_xticklabels([])
#ax2.set_yticklabels([])
plt.setp(ax2.get_xticklabels(),visible=False)
plt.setp(ax2.get_xticklines(),visible=False)

pos2=ax2.pcolor(1-M,cmap=plt.cm.hot,edgecolor='k')
fig.colorbar(pos2, ax=ax2)

###
plt.show()
#plt.savefig("test.png", dpi=150)
