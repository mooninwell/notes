Date: 20190307，系统：Window 7 SP1 64bit

## 安装 Juptyer notebook

---

首先需要安装Python环境, 见 python-installation.md

---

### Anaconda 环境

Anaconda 自带 Jupyter 环境

使用Anaconda程序菜单栏 Anaconda Navigtor 或 Jupyter notebook 打开即可。

Anaconda中修改Jupyter Notebook默认工作路径的方法如下：

修改jupyter notebook.link： 右键属性->快捷方式->目标(T)中的命令行最后的%USERPROFILE%改成希望打开的工作路径。

这个jupyter notebook.link文件可以在Anaconda程序菜单栏或
`C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Anaconda3 (64-bit)`
这是Windows 7的位置. 如果是Windwos 10, 可能位于
`C:\Users\<user name>\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Anaconda3 (64-bit)`  
中找到，注意把<user name>换成用户名。

----
----
### Python 环境安装 juptyer notebook

Python 版本 3.7.2 64bit

下面介绍 pip 安装 juptyer notebook

`pip install jupyter notebook`

pip 可以用-i 选项选择国内源，比如

`pip install jupyter notebook -i https://pypi.tuna.tsinghua.edu.cn/simple`

安装好后再cmd窗口输入

`jupyter notebook`

将会通过浏览器自动运行，注意运行时cmd窗口不能关闭。输入两次ctrl+C结束运行。

#### 修改Jupyter Notebook默认工作路径

在cmd中输入`jupyter notebook --generate-config`

修改`.jupyter\jupyter_notebook_config.py`文件 
```## The directory to use for notebooks and kernels. 
c.NotebookApp.notebook_dir = 'E:\python-learning'  #必须删除，且前面不能留空格。 
```
python-learning 文件夹必须提前新建，如果没有新建，Jupyter Notebook会找不到这个文件，会产生闪退现象。

#### 修改Jupyter Notebook默认使用的浏览器
修改`.jupyter\jupyter_notebook_config.py`文件 
```
import webbrowser
webbrowser.register("chrome",None,webbrowser.GenericBrowser(u"C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe"))
c.NotebookApp.browser = 'chrome'
```
把上面的chrome和路径换成你需要的浏览器。

---

#### 问题

在3月5日安装的jupyter notebook在运行时出现问题，kernel连接不上，出错的关键信息是
```
.../site-packages/notebook/base/zmqhandlers.py:284: RuntimeWarning: coroutine 'get' was never awaited super(AuthenticatedZMQStreamHandler, self).get(*args, **kwargs)
```
通过 stackoverflow.com 搜索发现是tornado 6.0 新版本的问题，
可以通过下面的方式安装5.1.1版本的tornado 
```
pip install --upgrade tornado==5.1.1
```
估计下个版本的tornado会修复这个问题
