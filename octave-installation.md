Date: 20190307，系统：Window 7 SP1 64bit

MATLAB 是一个功能强大的商业数学软件. GNU Octave是一款开源且免费的软件, 它可以代替MATLAB的大多数功能，但不包括Simulink组件功能。

### 安装 GNU Octave
在官网https://www.gnu.org/software/octave/ -> Download->Windows  点击下载安装文件 octave-5.1.0-w64-installer.exe (286 MB). 安装后大概占1.7 GB.

安装时建议自定义安装目录, 最好是英文, 不含空格等特殊字符. 我的安装路径是 E:\Octave.

安装好后再开始菜单点击 Octave-5.1.0.0 (GUI) 启动Octave 图像化界面,
Octave-5.1.0.0 (CLI) 是命令行界面. Octave 还自带 MINGW64 Bash shell. 

#### 更改 Octave 启动时的工作目录

修改Octave启动的配置文件octaverc, 位于
> E:\Octave\Octave-5.1.0.0\mingw64\share\octave\site\m\startup\

在文件中添加内容:
```
setenv("USERPROFILE", "E:\\Octave\\Octave-test");
cd(getenv("USERPROFILE"));
```
参考时注意替换相应的路径.

另一个方法: 修改快捷方式 Octave-5.1.0.0 (GUI).link, 右键->属性-> 起始位置: 把%USERPROFILE%改成E:\Octave\Octave-test

Octave-5.1.0.0 (GUI).link位于
```
C:\ProgramData\Microsoft\Windows\Start Menu\Programs\GNU Octave 5.1.0.0
```
这是Windows 7的位置. 如果是Windwos 10, 可能位于
```
C:\Users\<user name>\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\GNU Octave 5.1.0.0
```
中找到，注意把<user name>换成用户名。

也可以把这个快捷方式拷贝到其它地方, 甚至不同的快捷方式有不同的起始工作目录.

#### Jupyter notebook 配合 Octave Kernel
需要Python 环境和安装好Jupyter notebook, 见 python-installation.md和jupyter-notebook-installation.md

Octave Kernel 的官方见 https://github.com/Calysto/octave_kernel

- Anaconda环境, 使用conda安装, 在cmd中运行:
```
conda config --add channels conda-forge
conda install octave_kernel
conda install texinfo # For the inline documentation (shift-tab) to appear.
```
如果没有将Anaconda添加至环境变量, 需要在conda promot运行conda命令.

- 官网Python环境, 使用pip安装, 在cmd中运行
```
pip install octave_kernel
```
pip可以使用国内源, 比如
```
pip install octave_kernel -i https://pypi.tuna.tsinghua.edu.cn/simple
```
在用户的环境变量表中添加一个变量名为`OCTAVE_EXECUTABLE`, 值为`E:\Octave\Octave-5.1.0.0\mingw64\bin\octave-cli.exe`的环境变量.

参考时注意替换相应的路径.

在cmd 中运行
```
jupyter notebook
```
浏览器中可以新建 octave kernal 的ipynb文件.
一个示例文件是:
https://nbviewer.jupyter.org/github/Calysto/octave_kernel/blob/master/octave_kernel.ipynb