Date: 20190307，系统：Window 7 SP1 64bit

### Python 环境搭建 

一般有两种方式, 安装集成的Python发行版本 Anaconda, 包含了conda、Python等180多个科学包及其依赖项, 或官网下载Python安装, 再根据需要安装更多的库.

---

#### 安装Anaconda 
在Anaconda官网 (https://www.anaconda.com/download/) 直接根据电脑系统类型进行下载不同的版本. 本环境使用  Anaconda 64-Bit Graphical Installer (614.3 M ),安装文件名: Anaconda3-2018.12-Windows-x86_64.exe. 安装后大概占用 1.6 G 空间.

建议选择只为“我这个用户”安装, 自定义安装路径, 路径最好是全英文的, 不包含空格等特殊字符. 
“Advanced Installation Options”默认不勾选“Add Anaconda to my PATH environment variable.”（“添加Anaconda至我的环境变量.”）. 并且提示如果勾选, 则将会影响其他程序的使用, 除非你打算使用多个版本的Anaconda或者多个版本的Python, 否则便勾选“Register Anaconda as my default Python 3.7”, 

如果你不打算安装多个Python版本, 这里还是建议将Anaconda添加至环境变量.
如果安装时没有勾选相应的选项, 可以在 计算机->右键 属性->高级系统设置->高级->环境变量->用户环境变量, 新建和编辑PATH变量名, 添加Anaconda安装路径, 比如我的安装路径如下: 
> E:\Anaconda\Scripts;E:\Anaconda; 

修改环境变量的时候要注意, 如果杀毒软件提示警告信息, 要点击允许, 否则可能导致环境变量被删除. 如果是手动添加环境变量, 在删除Anaconda时也需要手动删除环境变量.

添加至环境变量的好处是可以在cmd中运行python相关命令, 否则要使用Anaconda, 则通过开始菜单打开Anaconda Navigator或者在开始菜单中的“Anaconda Prompt”中进行使用.

---
#### 官网安装Python
在Python的官方https://www.python.org/downloads/windows/点击Latest Python 3 Release - Python 3.7.2, 往下拉选着 Windows x86-64 executable installer, 下载 python-3.7.2-amd64.exe (24.9 MB). 安装后大概占用 200 M的空间. 

安装时选择 Customize installation, 自定义安装路径, 建议勾选Add Python 3.7 to PATH. 

如果安装时没有勾选添加环境变量, 可以按照关于Anaconda的方法添加到用户环境变量PATH
> E:\Python\Python37\Scripts;E:\Python\Python37;

---
#### 测试
在 cmd 输入
> python -V

查看安装的python版本, 注意是大写字母V.

在 cmd 输入
> python

进入python 程序. 再输入
> print('Hello world')

敲回车, 就可以看到执行结果了.

输入
> exit()

退出python程序.

---
注意不要同时添加Anaconda环境和官网python环境. 如果需要不同版本的python环境, 可以使用Anaconda的虚拟环境.