### Fonts and TeX
https://www.tug.org/fonts/

### The LaTeX Font Catalogue
www.tug.dk/FontCatalogue/

### A Survey of Free Math Fonts for TEX and LATEX 
By Stephen G. Hartke

The PracTEX Journal 

TPJ 2006 No 01, 2006-02-15

Article revision 2006-02-03

http://tug.org/pracjourn/2006-1/hartke/

仅供参考，里面可能有不少字体包已过时

### 如何在LaTeX数学模式中更好地使用粗体？
https://www.zhihu.com/question/25290041

基本的切换为数学粗体的命令是 \boldmath，它通常被定义为 \mathversion{bold}

恢复正常粗细是 \unboldmath 即 \mathversion{normal}

amsmath 宏包（更确切地说是子包 amsbsy）提供了 \boldsymbol 命令

amsmath 还提供了 \pmb 命令来提供伪粗体（poor man's bold）的符号

推荐的宏包——bm, bm 宏包提供的主要命令是 \bm，例子\bm{\alpha}, 它和 amsmath 的 \boldsymbol 一样，把参数中的数学符号用粗体输出。

在高质量的排版输出中，应该避免使用伪粗体。而要避免伪粗体，没有其他路子可走，就是选用字重齐全的高质量数学字体。TeX 系统自带的 CM 字体缺少部分粗体符号，但可以使用符号更为齐全的 newtxmath（Times 风格，配合 newtxtext 使用，前身是 txfonts）、stix（Times 风格，STIX 系列字体以大而全著称）、pxfonts（Palatino 风格）、mathdesign（本身只包含符号，有不同选项用来配合 Utopia、Garamond、Charter 字体）、MnSymbol（只包含符号）、fdsymbol（只包含符号）、lucidabr（Lucida Bright 商业字体）等字体包。一些专业的商业字体，如 MathTime Pro 的两个版本，甚至有 normal、bold、heavy 三种粗细可以使用（bm 宏包为最后一种粗细提供了 \hm 命令）。

不推荐使用mathptmx

XeLaTeX可以用TrueType/OpenType数学字体，因此微软/Ascender的Cambria Math就可以用了。此外，开源的TrueType/OpenType数学字体还有Asana Math (用的是URW Paladio）和另外几个基于URW开源字体制作的数学字体，以及Computer Modern本身的otf化，以及STIX。

### Replacement for MathTime Pro
https://tex.stackexchange.com/questions/257288/replacement-for-mathtime-pro

I would replace Math Times Pro with newtx. By default, it uses a free clone of Times, TeX Gyre Termes, but you can optionally use Libertine,Minion, Garamondx, Baskervaldx or Utopia.

### Top 10 LaTeX Fonts  assembled by Jaap Joris Vens
https://r2src.github.io/top10fonts/

1. Comupter Modern, default
2. Ko-Fonts
\usepackage{kpfonts}
3. GFS Didot,
one of the following \usepackage{gfsartemisia}  \usepackage{gfsbodoni}  \usepackage{gfsdidot}   \usepackage{gfsneohellenic} 
4. Utopia,
\usepackage{fourier} and \usepackage[utopia]{mathdesign}
5. Venturis ADF,
One of the following: \usepackage{venturis} \usepackage{venturis2} \usepackage{venturisold}
6. Linux Libertine
\usepackage{libertine}
7. TeX Gyre Collection
 - Bonum/Bookman \usepackage{tgbonum}
 - Pagella/Palladio/Palatino \usepackage{tgpagella}
 - Schola/Century Schoolbook \usepackage{tgschola}
 - Termes/Nimbus Roman/Times Roman \usepackage{tgtermes}
8. URW Antiqua
9. Bitstream Vera
\uspackage{bera} or \usepackage{dejavu}
10. Boisik
\usepackage{biosik}

### Suggest a “nice” font family for my basic LaTeX template (text and math)
https://tex.stackexchange.com/questions/59702/suggest-a-nice-font-family-for-my-basic-latex-template-text-and-math
- Computer Modern
- lmodern, Latin Modern, Very much like Computer Modern, but with many more glyphs.
- mathpazo -- based on Hermann Zapf's Palatino font
- newpxtext and newpxmath -- also based on Zapf's Palatino font. The packages are based on (and constitute noticeable improvements) on Young Ryu's older pxfonts font package.
- kpfonts -- "Kepler" fonts. A very nice set of fonts, based originally on Palatino, but highly evolved and outfitted with many special features. 
- mathptmx -- based on the Times Roman font (obsolete).
- stix2, The stix font package provides yet another Times clone (stix is obsolete)
- txfonts -- another package based on Times Roman, by Young Ryu
- The Linux Libertine font family. If you like this text font and wish to employ it with the newtxmath package, be sure to load the newtxmath package with the libertine option
- The "TeX-Gyre" font families Termes (a Times Roman clone), Pagella (a Palatino clone), and Schola (a Century Schoolbook clone); one would load the packages tgtermes, tgpagella, and tgschola, respectively, to access these fonts.
- if one uses the Palatino text font together with the AMS Euler (eulervm) math font.
```
\usepackage[T1]{fontenc}
\usepackage[tracking]{microtype}
\usepackage[sc,osf]{mathpazo}   % With old-style figures and real smallcaps.
\linespread{1.025}  % Palatino leads a little more leading
% Euler for math and numbers
\usepackage[euler-digits,small]{eulervm}
```
- MinionPro by Adobe, 
Read the documentation for the MinionPro package. Add the following to your preamble:
'''
\usepackage[footnotefigures]{MinionPro}
\input{glyphtounicode}
\pdfgentounicode=1
\renewcommand{\scdefault}{ssc}
\usepackage[scaled=0.85]{luximono} % Monospace font
\usepackage[letterspace=160,babel=true,tracking=true,kerning=true]{microtype}
'''
- Cochineal, newtxtt, and Cabin, with newtxmath
'''
\usepackage[p,osf]{cochineal}
\usepackage[scale=.95,type1]{cabin}
\usepackage[cochineal,bigdelims,cmintegrals,vvarbb]{newtxmath}
\usepackage[zerostyle=c,scaled=.94]{newtxtt}
\usepackage[cal=boondoxo]{mathalfa}
'''
- garamondx, zlmtt, and Source Sans Pro with newtxmath

Here are two options using garamondx, which is free but not included in texlive out-of-the-box, although I think it might be available in MikTeX. It has to be downloaded separately if you're running texlive, which the getnonfreefonts program will do for you.
```
\usepackage[full]{textcomp}
\usepackage{garamondx}
\usepackage[scaled=1.01]{zlmtt}
\usepackage{sourcesanspro}
\usepackage[garamondx,cmbraces]{newtxmath}
\useosf
```
- garamondx, inconsolata, and Fira Sans with mathdesign
```
\usepackage[garamond]{mathdesign}
\usepackage[full]{textcomp}
\usepackage{garamondx}
\usepackage[varqu,varl,var0,scaled=0.97]{inconsolata}
\usepackage{FiraSans}
```
- Bitstream Charter. \usepackage{charter}, 
Charis SIL is a free derivate that provides more glyphs. 
\usepackage[charter]{mathdesign}
\usepackage{XCharter}

- mathastext with fbb
'''
\usepackage{fbb}
\usepackage[libertine]{newtxmath} 
\usepackage[italic]{mathastext}
\MTsetmathskips{f}{5mu}{1mu} # add space to compensate for the large f-italic in fbb
'''
- A sample
```
%%%%     FONTS PACKAGE OPTIONS in the Preamble  %                        
%%%%     always replace default Computer Modern with Latin Modern [T1] encoding:
\renewcommand{\ttdefault}{lmtt} % MONO Latin Modern Font % T1 encoding of cmtt font style  
\renewcommand{\rmdefault}{lmr} % SERIF Latin Modern Font % T1 encoding of cmr font style
\renewcommand{\sfdefault}{lmss} % SANS Latin Modern Font % T1 encoding of cmss font style
%  %  %   %   %   %   %   %   %   %   %   %   %   %   %   % 
%  %  % PSNFSS \ssfamily fonts (SANS)
\usepackage{helvet} % PSNFSS Font, in every TeX distribution 
\usepackage{avant}  % PSNFSS Font, in every TeX distribution 
%  %  % Extended \ssfamily (Sans) fonts; load extraFonts option from TeX                    
\usepackage[scaled=0.88]{berasans}% package has a handy scaling option
\usepackage{libris}   % a nice, almost handwritten calligraphic look
\usepackage{biolinum} % included with the {Libertine} font package
\usepackage{iwona}
\usepackage{paratype} 
%%%  END OF SANS SERIF FONT PACKAGES 
%  %  %  %   %   %   %   %   %   %   %   %   %   %   %   %   %   %
%  %  % PSNFSS \rmfamily fonts (SERIF) 
\usepackage{mathptmx}  % Times % PSNFSS Font, in every TeX distribution %
\usepackage{charter}   % Bitstream Charter % PSNFSS Font, in every TeX distribution %
\usepackage{mathpazo}  % Palatino % PSNFSS Font, in every TeX distribution 
\usepackage{bookman}   % Bookman % PSNFSS Font in every TeX
\usepackage{chancery}  % Zapf Chancery % PSNFSS Font in every TeX, a Calligraphic Font
\usepackage{newcent}   % New Century Schoolbook % PSNFSS Font in every TeX distribution 
% To replace it's Avant Garde sans add this:% \renewcommand{\sfdefault}{xxx}
%  %  % Extended \rmfamily (SERIF) fonts; load extraFonts option from TeX
\usepackage[scaled=0.88]{beraserif}% package has a handy scaling option
\usepackage{XCharter} % Bitstream's Charter extended with many style varieties 
\usepackage{fouriernc}% Century Schoolbook % compact and lighter than New Century Schoolbook 
\usepackage{tgschola} % TeX Gyre Schola, New Century Schoolbook with many font style varieties 
\usepackage{tgtermes} % TeX Gyre Termes, Times with many font style varieties 
\usepackage{tgbonum}  % TeX Gyre Bonum, Bookman with many font style varieties 
\usepackage{tgpagella}% TeX Gyre Pagello, a Palatino font with many font style varieties 
\usepackage{fourier}  % Utopia, package {utopia} is obsolete
\usepackage{txfonts}  % TX Serif and Sans (Helvetica)
\usepackage{kpfonts}  % KP Serif and Sans, large variety of font styles
\usepackage{libertine}% Libertine + Linux Biolinum (Extra TeXLive fonts) 
\usepackage{fbb}      % A Garamond Font (Bembo) with many font styles
%%% END SERIF FONTS
%%% NOTE!!! Be sure to comment out all but ONE Serif and ONE Sans
%%% from the package selections above.
%  %  %   %   %   %   %   %   %   %   %   %   %   %   %
%  %  %   %   %   %   %   %   %   %   %   %   %   %   % 
%  %  % Load Math Support if necessary
%
\usepackage[myFontPackage]{mathdesign}
%
%
%%% END OF FONTS In the Preamble 
```
### Examples in `newtxdoc.pdf`

注意: `newtxtext` 使用`looser`选项, 词间距会宽一点.   
`newtxmath`会自动加载`amsmath`宏包, `amsthm`要放在`newtxmath`之前, `bm`要放在`newtxmath`之后

Spacing issues  
|                                | txfonts | Termes |
| ------------------------------ | ------- | ------ |
| fontdimen2 (interword space)   | .25em   | .25em  |
| fontdimen3 (interword stretch) | .15em   | .2em   |
| fontdimen4 (interword shrink)  | .06em   | .1em   |

Option `tighter` sets the three fontdimen values to those of txfonts.   
Option `looser` sets the three fontdimen values to `{.3em,.2em,.1em}` respectively.   

```
\usepackage[LGR,T1]{fontenc} % spell out all text encodings used 
\usepackage[utf8]{inputenc} % 
\usepackage{substitutefont} % so we can use fonts other than those in babel 
\usepackage[greek.polutoniko,english]{babel} 
\usepackage[largesc,osf]{newtxtext} % 
\usepackage[varqu,varl]{zi4}% inconsolata 
\usepackage{cabin}% sans serif 
\usepackage[vvarbb]{newtxmath} 
\useosf % use oldstyle figures except in math 
\substitutefont{LGR}{\rmdefault}{Tempora} % use Tempora to render Greek text 
```

```
\usepackage[osf]{erewhon} %extension of Utopia 
\usepackage[varqu,varl]{inconsolata} % sans typewriter 
\usepackage[scaled=.95]{cabin} % sans serif 
\usepackage[utopia,vvarbb]{newtxmath}
```

```
% The next line is no longer needed, as newtxmath Requires it 
%\usepackage{amsmath}% loads amstext, amsbsy, amsopn but not amssymb 
\usepackage{newtxmath}
```

If you wish to use `\usepackage{amsthm}`, place it before loading `newtxmath` or the result will be   
```
! LaTeX Error: Command \openbox already defined.
```

```
\usepackage[osf]{newtxtext} % T1, lining figures in math, osf in text 
\usepackage{textcomp} % required for special glyphs 
%\usepackage{amsmath} % not needed, as it is Required by newtxmath 
\usepackage[varvf,vvarbb]{newtxmath} 
\usepackage{bm} % load after all math to give access to bold math %\useosf %no longer required if osf specifie
```

```
\usepackage[lining,semibold]{libertine} % a bit lighter than Times--no osf in math 
\usepackage[T1]{fontenc} % best for Western European languages 
\usepackage{textcomp} % required to get special symbols 
\usepackage[varqu,varl]{inconsolata}% a typewriter font must be defined 
\usepackage{amsmath,amsthm}% must be loaded before newtxmath 
\usepackage[libertine,vvarbb]{newtxmath} 
\usepackage[scr=rsfso]{mathalfa} 
\usepackage{bm}% load after all math to give access to bold math 
%After loading math package, switch to osf in text. 
\useosf % for osf in normal text
```

```
\usepackage[lining,semibold,scaled=1.05]{ebgaramond} % Latex BOLD renders with ebgaramond semibold 
\usepackage[T1]{fontenc} % best for Western European languages 
\usepackage{textcomp} % required to get special symbols 
\usepackage[varqu,varl]{inconsolata}% a typewriter font must be defined 
\usepackage{amsmath,amsthm}% must be loaded before newtxmath 
\usepackage[ebgaramond,vvarbb,subscriptcorrection]{newtxmath} % STIX Bbb 
\usepackage{bm}% load after all math to give access to bold math
```

#### Tight or loose?      
Times style fonts are too tight for screen reading, but `stix2` seems a litter looser. Palatino stlye may be better, using `newpxtext` package.
