### Pandoc  
Pandoc 格式转化工具, 将LaTeX公式转化为docx自带的公式.

### Mathtype version 6.5 or later 
You can type TeX directly into your Microsoft Word document. 
When you are done, type Alt+\ (Toggle TeX) to convert it to a MathType equation. 
Later, if you want to edit the equation's TeX code, just type Alt+\ again. 
The Toggle TeX command allows you to switch between TeX and MathType views of the equation.

### 商业软件     
1. `Word2TeX` and `TeX2Word`  https://www.chikrii.com/
2. `Word-to-LaTeX` and `LaTeX-to-Word` https://grindeq.com

### Powerpoint 
1. IguanaTex
2. Aurora 

### Office 365 版本 1707或者之后的
直接在word里按照latex的方式输入, 选择LaTeX模式。
搜索框里面输入`公式选项`, 
公式选项选择字体Cambria Math, 可以安装TeXlive中的数学字体到系统, 比如Latin Modern Math等

### 在线LaTeX公式编辑器
1. https:/www.codecogs.com/latex/eqneditor.php
2. https://latex.vimsky.com/

### 手绘符号识别为LaTeX命令
http://detexify.kirelabs.org/classify.html

### 参考
https://www.zhihu.com/question/40763253