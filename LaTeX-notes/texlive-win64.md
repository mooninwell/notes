https://tex.stackexchange.com/questions/180490/will-texlive-2014-be-64-bit

By Akira Kakuto:

We don't release 64bit Windows native binaries as the official TeX Live, but users can install them by downloading

CTAN/systems/win32/w32tex/TLW64/tl-win64.zip

CTAN/systems/win32/w32tex/TLW64/00README.TLW64

Please read 00README.TLW64 carefully.

or 

 [ ] x86_64-cygwin    x86_64 with Cygwin