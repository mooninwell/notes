### 方法一
```
\documentclass[a4paper,12pt]{article}
\usepackage{xcolor}
\usepackage{pdfpages}
\usepackage{graphicx}
\usepackage[left=0cm,right=0cm,top=0cm,bottom=0cm]{geometry}
\setlength\parindent{0pt}
\begin{document}

\vbox{\includepdfmerge{test.pdf,1}}%
\setlength{\unitlength}{1mm}%
\vfill
\begin{picture}(200,200)(0,0)%
\put(0,0){0,0}
\put(100,100){\textcolor{red}{hello}}%
\put(200,200){0,0}
\end{picture}%

\newpage 

\vbox{\includepdfmerge{test.pdf,1}}%
\vfill
\begin{picture}(200,200)(0,0)%
\put(0,0){0,0}
\put(100,100){\includegraphics[scale=1.2]{TeXworks.png}}%
\put(200,200){0,0}
\end{picture}%

\end{document} 
```
From: LaTeX对插入pdf页进行编辑
https://blog.csdn.net/e111r222/article/details/8676714

### 方法二

```
\usepackage{eso-pic}
\newcommand\BackgroundPic{
\put(16.57, 23.19){
\parbox[t]{\textwidth}{
\includegraphics[scale=1,keepaspectratio]{logo_jnm.png}%
}
}
}

正文紧接\begin{document}后面写
\AddToShipoutPicture*{\setlength{\unitlength}{1cm}\BackgroundPic}
具体来说就是先声明一个\BackgroundPic命令，这个命令在16.57， 23.19的位置放一个宽为\textwidth的parbox，
里面放上你要的图片Logo。这个Logo我是从Doc模板里面截出来的，按Word的 100%显示比较缩放成一丝不差，
所以scale=1并且keepaspectratio。注意那个\unitlength只有在正文区可以声明，所以导言区的16.57和23.19没写单位。
```
From: latex 插入Logo
https://blog.csdn.net/th_num/article/details/53303762

### 额外增加页码设置
```
\documentclass[a4paper,12pt]{article}
\usepackage{pdfpages}
\usepackage{lastpage}
\usepackage[left=0cm,right=0cm,top=0cm,bottom=0cm]{geometry}
\usepackage{eso-pic}
\newcommand\AddPageNumber{
\put(10, 2){Page \thepage\ of \pageref{LastPage}}
}

\setlength\parindent{0pt}

\begin{document}

\AddToShipoutPictureFG{\setlength{\unitlength}{1cm}\AddPageNumber}

\includepdfmerge{test.pdf,1-2}%

%\newpage 

\includepdfmerge{test.pdf,1}

\end{document} 
```

### pdfpages 使用
```
\documentclass{article}
\pdfcompresslevel9
\paperwidth 21cm    \pdfpagewidth\paperwidth
\paperheight29.7cm  \pdfpageheight\paperheight
\usepackage{pdfpages}
\begin{document}
\includepdf{output_tmp.pdf}
\end{document}
```
