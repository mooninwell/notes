### PDF文件上进行标注的方法：
1. 使用onenote虚拟打印机，打印至onenote
2. Inkodo 导入pdf
3. Drawboard PDF - Read, edit, annotate
4. Inky - PDF reader & ink annotation
5. PDF Annotate & Fill
6. PDF Markup 
7. Microsoft Edge, 83+, pdf viewer

### 格式转换
1. a2ping，sam2p：各种图片格式转化，jpeg, png, ps, PDF 等，一次只能处理一张图或一页PDF
2. pdfpages：PDF文件合并于分割
3. pdfjam：Shell scripts interfacing to pdfpages
4. ImageMagick：图片格式转化大全
5. pdftk：合并、分割、压缩
6. MuPDF Command Line Tools: 将页面转为图像等文件, 合并等
7. Xpdf，XpdfReader: PDF viewer and commands
    - pdftotext: converts PDF to text
    - pdftops: converts PDF to PostScript
    - pdftoppm: converts PDF pages to netpbm (PPM/PGM/PBM) image files
    - pdftopng: converts PDF pages to PNG image files
    - pdftohtml: converts PDF to HTML
    - pdfinfo: extracts PDF metadata
    - pdfimages: extracts raw images from PDF files
    - pdffonts: lists fonts used in PDF files
    - pdfdetach: extracts attached files from PDF files