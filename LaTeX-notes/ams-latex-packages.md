What does each AMS package do?
https://tex.stackexchange.com/questions/32100/what-does-each-ams-package-do

* amsmath
    * amstext
        * amsgen
    * amsbsy
        * amsgen
    * amsopn
        * amsgen
* amssymb
    * amsfonts
        * eufrak
* amsthm
* eu­cal
* eu­script 

The 'amssymb' package is redundant when you are using the 'stix' package

If one of the document classes of the AMS-collection (amsbook, amsart) is being used, there's no need to load amsmath, or amsthm; amssymb will have to be explicitly loaded.

mathtools loads amsmath, corrects some bugs, and provides additional useful features, many of which are extensions of the existing amsmath syntax.

Other packages: amscd, amsrefs

amsrefs includes am­sx­port, ifoption, mathscinet, pcatcode, rkeyval, textcmds

mathscinet provides some command used in the website mathscinent, such as \cprime 

ltb2bib – Converts amsrefs' .ltb bibliographical databases to BibTeX format

lt­b2bib is the re­verse of the "am­sx­port" op­tion in am­srefs.

mathrsfs pro­vides a \math­scr com­mand, rather than over­writ­ing the stan­dard \math­cal com­mand, as in calrsfs. 

note that eucal and euscript are identical, except that they use different commands (\mathcal and \mathscr)
\usepackage[mathscr]{euscript}