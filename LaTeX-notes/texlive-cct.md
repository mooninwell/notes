TeXlive 不支持CCT, 但各种原因可能需要编译旧格式的文档, 从网上找到下面的资源.

LaTeX 技巧 802：国内期刊 CCT 模板编译经验 
https://www.latexstudio.net/archives/3608.html

下载 CCT-full.zip, 解压后将CCT_TDS.zip再解压, 
将tex和font文件夹的内容放到本地texlive安装路径texmf-local相应的目录. 用`texhash`刷新文件库.

如果缺少相应的宏包, 也可按照上面的方式操作.

在`\documentclas` 里文档类选项，从`twoside`更换为`twoside, CJK`.   
如果使用 Windows 系统，则在`\begin{document}`之前添加一行命令`\AtBeginDvi{\input{zhwinfonts}}`.

编译出错   
`! No room for a new \count .`
添加`\usepackage{etex}`, 
最好加载第一个宏包位置

可以用`pdflatex`, 或`latex`编译, 使用的是`zhmetrics/zhwinfonts.tex`支持中文. 

------------------- 
CCT文件经常为GB2312格式,  texstudio支持不好, 可以用 Notepad++, NPPExec 插件允许LaTeX命令.   
```
NPP_SAVE
cd $(CURRENT_DIRECTORY)
pdflatex.exe -synctex=1 $(NAME_PART).tex
```