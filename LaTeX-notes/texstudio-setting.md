Options->Configure Texstudio...  
General  
  Appearance  
    Font: 微软雅黑   
    Font size: 10  
    Language: zh_CN

选项->设置TeXstudio  
常规  
取消自动检查更新

命令  
默认编辑器: Latexmk

编辑器  
字号: 12

语法高亮  
背景-->背景色: HTML #cce8cf

补全  
永久激活补全文件  
勾选一些常见的宏包`amsart, asmbsy, amsfonts, asmmath, amsopn, amssymb, amsthm,ctex,ctexart,hyperref,verbatim`   
注意勾选`class-beamer, class-book`可能导致`\ref`的自动效果消失 (TeXstudio 2.12.22: 提示标签缺失, label missing)

语言检查  
拼写检查  
拼写字典路径: C:\Program Files (x86)\texstudio\dictionaries  
默认语言: en_US

预览  
命令: 用dvipng预览 --follow (parallel)

显示高级选项  
内置PDF->背景色 #cce8cf

texstudio编译时文件路径不能包含中文, 否则PDF转跳到源文件功能失效, 原因是 synctex.gz文件中文路径乱码.