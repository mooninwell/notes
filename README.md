# Notes

#### 介绍
一些笔记, 参考时注意软件版本和时效性.

---

#### Python

- 书

   1.《Python零基础入门学习-水木书荟》

   2.《像计算机科学家一样思考Python 第2版》,[中文翻译网站](https://github.com/cycleuser/ThinkPython-CN)

- 网站

   1. [Python 基础教程](http://www.runoob.com/python3/python3-tutorial.html)

   2. [Python教程](https://www.liaoxuefeng.com/wiki/0014316089557264a6b348958f449949df42a6d3a2e542c000)

- 练习

   1. [Python 100例](http://www.runoob.com/python/python-100-examples.html)

   2. [Project Euler](https://projecteuler.net/archives)

##### NumPy, SciPy, Matplotlib, pandas

- 书

   1.《Python 3.0科学计算指南》

   2.《Python数据可视化之matplotlib实践》

- 网站

   1. [NumPy 教程](http://www.runoob.com/numpy/numpy-tutorial.html)

   2. [NumPy 中文文档](https://www.numpy.org.cn/)

   3. [Matplotlib中文文档](https://www.matplotlib.org.cn/)

   4. [Pandas 中文文档](https://www.pypandas.cn/)

#### Jupyter notebook

[https://jupyter.org/](https://jupyter.org/)

[Jupyter Notebook介绍、安装及使用教程](https://www.jianshu.com/p/91365f343585)

[Jupyter Notebook 初级教程](https://www.jianshu.com/p/1d213aed709e)

---

#### MATLAB/ GNU Octave

《MATLAB基础与应用教程（第2版）》蔡旭晖，刘卫国，蔡立燕 著 

https://www.gnu.org/software/octave/

---

#### Markdown

[Markdown基本语法](https://www.jianshu.com/p/191d1e21f7ed)

---

#### Git

[Git教程](https://www.liaoxuefeng.com/wiki/0013739516305929606dd18361248578c67b8067c8c017b000)

---

#### LaTeX

- 书

   1. 《LaTeX入门》

   2. 《LaTeX2e 完全学习手册（第2版）》

- 网站
   1. [http://www.latexstudio.net/](http://www.latexstudio.net/)

   2. 一份不太简短的 LaTeX2ε 介绍【中文资料】（[lshort中文版](http://mirrors.ctan.org/info/lshort/chinese/lshort-zh-cn.pdf)）

   3. [LaTeX Notes 雷太赫排版系统简介](http://mirrors.ctan.org/info/latex-notes-zh-cn/latex-notes-zh-cn.pdf), 包太雷, 2013【中文资料】

- 在线编辑
   1. overleaf.com
   2. texpage.com
